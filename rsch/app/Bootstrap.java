import org.apache.log4j.Level;

import play.Logger;
import play.jobs.Job;
import play.jobs.OnApplicationStart;

@OnApplicationStart
public class Bootstrap extends Job{
	
	@Override
	public void doJob() throws Exception {

		System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

		Logger.log4j.getLogger("org.apache.axis").setLevel(Level.DEBUG);
		Logger.log4j.getLogger("org.apache.axis.transport").setLevel(Level.DEBUG); 
	}
}
