package controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mysql.fabric.xmlrpc.base.Array;
import com.siebel.hyundai.hmbamarocsisurveyinio.SurveyUpsertOutput;
import com.siebel.hyundai.hmbservicerequestio.UpsertServiceRequestOutput;
import com.sun.net.httpserver.Authenticator.Result;

import controllers.commons.BaseService;
import dto.RespostaPesquisaDTO;
import models.Alternativa;
import models.Aplicacao;
import models.Cor;
import models.CorpoVeiculo;
import models.Dealer;
import models.LogEnvioCotacaoMonteOSeu;
import models.LogEnvioCotacaoPesquisa;
import models.LogEnvioPesquisa;
import models.Marca;
import models.OutrosVeiculos;
import models.Pesquisa;
import models.Questao;
import models.Requisicao;
import models.Resposta;
import models.Veiculo;
import models.VersaoVeiculo;
import models.enums.QuestaoTipoEnum;
import play.Play;
import play.i18n.Messages;
import services.SiebelSSIntegration;
import services.SiebelSearchContactIntegration;
import services.SiebelSurveyIntegration;
import vo.AccountVO;
import vo.SurveyVO;

public class PesquisaController extends BaseService {
	
	public static void getPesquisaEnviada(String idSiebel, String tag) {
		JsonObject result = null;
		
		try {
			validation.required(idSiebel);
			
			if(validation.hasErrors()) {
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			Pesquisa pesquisa = Pesquisa.find("SELECT p FROM Pesquisa p WHERE p.tag = ?", tag).first();
			
			validaPesquisa(pesquisa);
			
			if(validation.hasErrors()) {
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			LogEnvioPesquisa log = LogEnvioPesquisa.find("SELECT l FROM LogEnvioPesquisa l WHERE l.idSiebel = ? and l.pesquisa.id = ?", idSiebel, pesquisa.id).first();
			
			boolean enviado = false;
			
			if(Objects.nonNull(log)) {
				enviado = true;
			}
			
			result = getSuccessResult();
			result.addProperty("enviado", enviado);
			
			if(log != null) {
				result.addProperty("protocolo", log.protocolo);
				result.addProperty("protocoloCotacao", Objects.nonNull(log.protocoloCotacao) && StringUtils.isNotBlank(log.protocoloCotacao) ? log.protocoloCotacao : "");
			} else {
				result.addProperty("protocolo", "");
				result.addProperty("protocoloCotacao", "");
			}
			renderJSON(result.toString());
		} catch (Exception e) {
			validation.addError("erro", e.getMessage());
			result = erroJsonRetorno();
			renderJSON(result.toString());
		}
	}

	public static void getPesquisa(String idSiebel, String tag, String primeira_resposta) {
		JsonObject result = null;
		
		try {
			validation.required(idSiebel);
			validation.required(tag);
			
			if(validation.hasErrors()) {
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			AccountVO accountVO = SiebelSearchContactIntegration.loadContactByIdToVO(idSiebel);
			
			if(Objects.isNull(accountVO)) {
				validation.addError("erro", "Não foi possível localizar o usuário.");
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			List<Marca> marcas = buscaMarcas();
			Dealer dealer = buscaDealerCotado(accountVO);
			
			Pesquisa pesquisa = Pesquisa.find("SELECT p FROM Pesquisa p WHERE p.tag = ?", tag).first();
			
			validaPesquisa(pesquisa);
			
			if(validation.hasErrors()) {
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			Resposta resposta = Resposta.find("SELECT r FROM Resposta r INNER JOIN r.questao q WHERE r.pesquisa.id = ? and r.id_siebel = ? ORDER BY q.ordem DESC", pesquisa.id, idSiebel).first();
			
			Questao questao = getQuestao(pesquisa, resposta);
			List<Alternativa> alternativas = getAlternativas(questao);
			String modeloCotado = accountVO.lastLead.model;
			
			render(idSiebel, tag, primeira_resposta, pesquisa, questao, alternativas, marcas, dealer, modeloCotado);
		} catch (Exception e) {
			validation.addError("erro", "Localized Message: " + e.getLocalizedMessage() 
										+ "\n Message: " + e.getMessage() 
										+ "\n Cause: " + e.getCause()
										+ "\n Stack Trance: " + e.getStackTrace()
										+ "\n Suppressed" + e.getSuppressed());
			result = erroJsonRetorno();
			renderJSON(result.toString());
		}
	}
	
	public static void proximaQuestao() {
		JsonObject result = null;
		
		try {
			String authorization = request.headers.get("authorization").value().replace("Bearer ", "");
			String chaveApp = request.headers.get("chave_app").value();
			String origem = request.headers.get("origem").value();
			
			String idSiebel = JsonParsersObjects.bodyToString(request.params.get("body"), "siebel_id");
			Long idPesquisa = bodyToLong(request.params.get("body"), "pesquisa_id");
			Long idQuestao = bodyToLong(request.params.get("body"), "questao_id");
			JsonArray array = bodyToList(request.params.get("body"), "alternativas");
			String texto = JsonParsersObjects.bodyToString(request.params.get("body"), "texto");
			String primeira_resposta = JsonParsersObjects.bodyToString(request.params.get("body"), "primeira_resposta");
			
			validation.required(idSiebel);
			validation.required(idPesquisa);
			validation.required(idQuestao);
			
			if(validation.hasErrors()) {
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
//			Aplicacao aplicacao = Aplicacao.find("select a from Aplicacao a where a.chaveApp = ? and a.nomeAplicacao = ?" , chaveApp, origem).first();
//			
//			if(Objects.isNull(aplicacao)) {
//				validation.addError("erro", "Credenciais inválidas");
//				result = erroJsonRetorno();
//				renderJSON(result.toString());
//			}
//			
//			Requisicao requisicao = Requisicao.find("SELECT r FROM Requisicao r WHERE r.token = ? and r.aplicacao.id = ? and r.isActive = ?", authorization, aplicacao.id, true).first();
//			
//			if(Objects.isNull(requisicao)) {
//				validation.addError("erro", "Token inválido!");
//				result = erroJsonRetorno();
//				renderJSON(result.toString());
//			}
//			
//			requisicao.isActive = false;
//			requisicao = requisicao.save();
			
			AccountVO accountVO = SiebelSearchContactIntegration.loadContactByIdToVO(idSiebel);
			
			if(Objects.isNull(accountVO)) {
				validation.addError("erro", "Não foi possível localizar o usuário.");
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}

			List<Marca> marcas = buscaMarcas();
			Dealer dealer = buscaDealerCotado(accountVO);
			
			Pesquisa pesquisa = Pesquisa.findById(idPesquisa);
			
			validaPesquisa(pesquisa);
			
			if(validation.hasErrors()) {
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			Questao questao = Questao.findById(idQuestao);
			
			if(Objects.isNull(questao)) {
				validation.addError("erro", "Falha no envio das informações, por favor tente novamente.");
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}

			List<Long> alternativas = converteJsonArray(array);
			
			if(Objects.isNull(alternativas) || alternativas.isEmpty()) {
				validation.addError("erro", "Falha no envio das informações, por favor tente novamente.");
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			for (Long id : alternativas) {
				Alternativa alternativa = Alternativa.findById(id);
				
				if(Objects.isNull(alternativa)) {
					validation.addError("erro", "Falha no envio das informações, por favor tente novamente.");
					result = erroJsonRetorno();
					renderJSON(result.toString());
				}
				
				salvaResposta(texto, idSiebel, pesquisa, questao, alternativa);
			}
			
			Questao proximaQuestao = null;
			List<Alternativa> alternativasProximaQuestao = new ArrayList<Alternativa>();
			
			if(questao.ultima) {
				proximaQuestao = new Questao();
			} else {
				Alternativa alternativa = Alternativa.findById(alternativas.get(0));
				proximaQuestao = Questao.findById(alternativa.questao_destino.id);
				alternativasProximaQuestao = getAlternativas(proximaQuestao);
				
				if(pesquisa.id == 1L && primeira_resposta.equalsIgnoreCase("nao")) {
					if((questao.id == 9L || questao.id == 10L ) && 
							(alternativa.id == 36L || alternativa.id == 37L || alternativa.id == 38L || alternativa.id == 41L || alternativa.id == 42L || alternativa.id == 43L)) {
						proximaQuestao = Questao.findById(14L);
						alternativasProximaQuestao = getAlternativas(proximaQuestao);
					}
				}
			}
			
			
			render(idSiebel, pesquisa, proximaQuestao, alternativasProximaQuestao, marcas, dealer);
		} catch (Exception e) {
			validation.addError("erro", e.getMessage());
			result = erroJsonRetorno();
			renderJSON(result.toString());
		}
	}
	
	public static void questaoAnterior(String idSiebel, Long pesquisa_id, Long questao_id, String primeira_resposta) {
		JsonObject result = null;
		
		try {
			validation.required(idSiebel);
			validation.required(pesquisa_id);
			validation.required(questao_id);
			
			if(validation.hasErrors()) {
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			AccountVO accountVO = SiebelSearchContactIntegration.loadContactByIdToVO(idSiebel);
			
			if(Objects.isNull(accountVO)) {
				validation.addError("erro", "Não foi possível localizar o usuário.");
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}

			List<Marca> marcas = buscaMarcas();
			Dealer dealer = buscaDealerCotado(accountVO);
			
			Pesquisa pesquisa = Pesquisa.findById(pesquisa_id);
			
			validaPesquisa(pesquisa);
			
			if(validation.hasErrors()) {
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			Questao questao = Questao.findById(questao_id);
			
			if(Objects.isNull(questao)) {
				validation.addError("erro", "Falha no envio das informações, por favor tente novamente.");
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			Alternativa alternativa = Alternativa.find("SELECT a FROM Alternativa a WHERE a.questao.id = ? AND a.ativo = ? AND a.removido = ?", questao.id, true, false).first();
			Questao questaoAnterior = Questao.findById(alternativa.questao_anterior.id);
			List<Resposta> respostas = Resposta.find("SELECT r FROM Resposta r WHERE r.id_siebel = ? and r.pesquisa.id = ? and r.questao.id = ?", idSiebel, pesquisa.id, questaoAnterior.id).fetch();
			
			if(pesquisa.id == 1L) {
				if(primeira_resposta.equalsIgnoreCase("sim")) {
					if(questao.id == 4L) {
						questaoAnterior = Questao.findById(3L);
						respostas = Resposta.find("SELECT r FROM Resposta r WHERE r.id_siebel = ? and r.pesquisa.id = ? and r.questao.id = ?", idSiebel, pesquisa.id, questaoAnterior.id).fetch();
					}
					
					if(respostas.isEmpty()) {
						if(questao.id == 7L) {
							questaoAnterior = Questao.findById(6L);
							respostas = Resposta.find("SELECT r FROM Resposta r WHERE r.id_siebel = ? and r.pesquisa.id = ? and r.questao.id = ?", idSiebel, pesquisa.id, questaoAnterior.id).fetch();
						}
					}
				} else if(primeira_resposta.equalsIgnoreCase("nao")) {
					if(questao.id == 4L) {
						questaoAnterior = Questao.findById(13L);
						respostas = Resposta.find("SELECT r FROM Resposta r WHERE r.id_siebel = ? and r.pesquisa.id = ? and r.questao.id = ?", idSiebel, pesquisa.id, questaoAnterior.id).fetch();
					} else if(questao.id == 11L) {
						questaoAnterior = Questao.findById(14L);
						respostas = Resposta.find("SELECT r FROM Resposta r WHERE r.id_siebel = ? and r.pesquisa.id = ? and r.questao.id = ?", idSiebel, pesquisa.id, questaoAnterior.id).fetch();
					}
					
					if(!respostas.isEmpty()) {
						for (Resposta resposta : respostas) {
							if(resposta.alternativa.id == 105L) {
								questaoAnterior = Questao.findById(15L);
								respostas = Resposta.find("SELECT r FROM Resposta r WHERE r.id_siebel = ? and r.pesquisa.id = ? and r.questao.id = ?", idSiebel, pesquisa.id, questaoAnterior.id).fetch();
							} else if(resposta.alternativa.id == 68L) {
								questaoAnterior = Questao.findById(14L);
								respostas = Resposta.find("SELECT r FROM Resposta r WHERE r.id_siebel = ? and r.pesquisa.id = ? and r.questao.id = ?", idSiebel, pesquisa.id, questaoAnterior.id).fetch();
							}
						}
					}
				}
			} else if (pesquisa.id == 2L) {
				for (Resposta resposta : respostas) {
					if(resposta.alternativa.id == 77L) {
						questaoAnterior = Questao.findById(20L);
						respostas = Resposta.find("SELECT r FROM Resposta r WHERE r.id_siebel = ? and r.pesquisa.id = ? and r.questao.id = ?", idSiebel, pesquisa.id, questaoAnterior.id).fetch();
					}
				}
			}
			
			List<RespostaPesquisaDTO> respondidas = new ArrayList<RespostaPesquisaDTO>();
			
			for (Resposta resposta : respostas) {
				RespostaPesquisaDTO dto = new RespostaPesquisaDTO();
				dto.id = resposta.alternativa.id;
				dto.texto = resposta.resposta;
				respondidas.add(dto);
			}
			
			List<Alternativa> alternativas =  getAlternativas(questaoAnterior);
			
			render(idSiebel, pesquisa, questaoAnterior, alternativas, respondidas, marcas, dealer);
		} catch (Exception e) {
			validation.addError("erro", e.getMessage());
			result = erroJsonRetorno();
			renderJSON(result.toString());
		}
	}
	
	@SuppressWarnings("removal")
	public static void envioPesquisa() {
		JsonObject result = null;
		
		try {
			String authorization = request.headers.get("authorization").value().replace("Bearer ", "");
			String chaveApp = request.headers.get("chave_app").value();
			String origem = request.headers.get("origem").value();
			
			String idSiebel = JsonParsersObjects.bodyToString(request.params.get("body"), "siebel_id");
			String protocoloCotacao = JsonParsersObjects.bodyToString(request.params.get("body"), "protocolo_cotacao");
			String utm_campaign = JsonParsersObjects.bodyToString(request.params.get("body"), "utm_campaign");
			String utm_content = JsonParsersObjects.bodyToString(request.params.get("body"), "utm_content");
			String utm_medium = JsonParsersObjects.bodyToString(request.params.get("body"), "utm_medium");
			String utm_source = JsonParsersObjects.bodyToString(request.params.get("body"), "utm_source");
			
			Long idPesquisa = bodyToLong(request.params.get("body"), "pesquisa_id");
			
//			validation.required(authorization);
//			validation.required(chaveApp);
//			validation.required(origem);
			validation.required(idSiebel);
			validation.required(idPesquisa);
			
			if(validation.hasErrors()) {
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
//			Aplicacao aplicacao = Aplicacao.find("select a from Aplicacao a where a.chaveApp = ? and a.nomeAplicacao = ?" , chaveApp, origem).first();
//			
//			if(Objects.isNull(aplicacao)) {
//				validation.addError("erro", "Credenciais inválidas");
//				result = erroJsonRetorno();
//				renderJSON(result.toString());
//			}
//			
//			Requisicao requisicao = Requisicao.find("SELECT r FROM Requisicao r WHERE r.token = ? and r.aplicacao.id = ? and r.isActive = ?", authorization, aplicacao.id, true).first();
//			
//			if(Objects.isNull(requisicao)) {
//				validation.addError("erro", "Token inválido!");
//				result = erroJsonRetorno();
//				renderJSON(result.toString());
//			}
//			
//			requisicao.isActive = false;
//			requisicao = requisicao.save();
			
			AccountVO accountVO = SiebelSearchContactIntegration.loadContactByIdToVO(idSiebel);
			
			if(Objects.isNull(accountVO)) {
				validation.addError("erro", "Não foi possível localizar o usuário.");
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			Pesquisa pesquisa = Pesquisa.findById(idPesquisa);
			
			validaPesquisa(pesquisa);
			
			if(validation.hasErrors()) {
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			List<Resposta> respostas = Resposta.find("SELECT r FROM Resposta r WHERE r.id_siebel = ? and r.pesquisa.id = ?", idSiebel, pesquisa.id).fetch();
			
			if(Objects.isNull(respostas) || respostas.isEmpty()) {
				validation.addError("erro", "Falha no envio das informações, por favor tente novamente.");
				validation.addError("erro", "Não goram localizadas as informações de respostas");
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			List<SurveyVO> lista = new ArrayList<SurveyVO>();
			
			int i = 1;
			for (Resposta resposta : respostas) {
				if(resposta.questao.tipo.equals(QuestaoTipoEnum.TEXTO) || resposta.questao.tipo.equals(QuestaoTipoEnum.DATA) || resposta.questao.tipo.equals(QuestaoTipoEnum.NUMERO) || resposta.questao.tipo.equals(QuestaoTipoEnum.SELECT)) {
					lista.add(new SurveyVO(new Long(i), i + "", resposta.questao.descricao, resposta.resposta, resposta.resposta));
				} else {
					if(pesquisa.id == 1L) {
						if(resposta.questao.id != 15L) {
							if(resposta.resposta != null && resposta.resposta.length() > 0) {
								lista.add(new SurveyVO(new Long(i), i + "" , resposta.questao.descricao, resposta.alternativa.descricao, resposta.resposta));								
							} else {
								lista.add(new SurveyVO(new Long(i), i + "" , resposta.questao.descricao, resposta.alternativa.descricao, ""));
							}
						} else {
							if(protocoloCotacao != null && protocoloCotacao.length() > 0) {
								lista.add(new SurveyVO(new Long(i), i + "", resposta.questao.descricao, "Cotação reenviada. Protocolo: " + protocoloCotacao + ".", ""));
							} else {
								lista.add(new SurveyVO(new Long(i), i + "", resposta.questao.descricao, "Cotação não enviada, não foi possível reenviar.", ""));
							}
						}
					} else {
						if(resposta.resposta != null && resposta.resposta.length() > 0) {
							lista.add(new SurveyVO(new Long(i), i + "" , resposta.questao.descricao, resposta.alternativa.descricao, resposta.resposta ));
						} else {
							lista.add(new SurveyVO(new Long(i), i + "" , resposta.questao.descricao, resposta.alternativa.descricao, ""));
						}
					}
				}
				i++;
			}
			
			Random random = new Random();
			int rand = random.nextInt(100000);
			SurveyUpsertOutput output = SiebelSurveyIntegration.sendSurveyItem(String.valueOf(rand), accountVO.cpf, accountVO.idSiebel, pesquisa.nome + " cod: " + String.valueOf(rand) , new Date(), null, lista);
			
			if(Objects.isNull(output)) {
				validation.addError("erro", "Falha no envio da pesquisa.");
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			if(Objects.nonNull(output) && !output.getErrorSpcCode().equals("0")) {
				validation.addError("erro", output.getErrorSpcCode() + ": Falha no envio da pesquisa.");
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			String protocolo = "";
			if(pesquisa.id == 1L) {
				protocolo = envioRequisicaoService(idSiebel, 92L, utm_campaign, utm_content, utm_medium, utm_source);
			} else if(pesquisa.id == 2L) {
				protocolo = envioRequisicaoService(idSiebel, 93L, utm_campaign, utm_content, utm_medium, utm_source);
			}
			
			if(validation.hasErrors()) {
				renderJSON(result.toString());
			}
			
			LogEnvioPesquisa log = new LogEnvioPesquisa(idSiebel, protocolo, protocoloCotacao, pesquisa);
			log = log.save();
			
			render(protocolo);
		} catch (Exception e) {
			validation.addError("erro", e.getMessage());
			result = erroJsonRetorno();
			renderJSON(result.toString());
		}
	}
	
	public static void reenvioCotacao() {
		JsonObject result = null;
		try {
			String authorization = request.headers.get("authorization").value().replace("Bearer ", "");
			String chaveApp = request.headers.get("chave_app").value();
			String origem = request.headers.get("origem").value();
			
			String idSiebel = JsonParsersObjects.bodyToString(request.params.get("body"), "siebel_id");
			String dealerCode = JsonParsersObjects.bodyToString(request.params.get("body"), "dealer_code");
			boolean novoDealer = JsonParsersObjects.bodyToBoolean(request.params.get("body"), "novo_dealer"); 
			
//			validation.required(authorization);
//			validation.required(chaveApp);
//			validation.required(origem);
			validation.required(idSiebel);
			validation.required(dealerCode);
			validation.required(novoDealer);
			
			if(validation.hasErrors()) {
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
//			Aplicacao aplicacao = Aplicacao.find("select a from Aplicacao a where a.chaveApp = ? and a.nomeAplicacao = ?" , chaveApp, origem).first();
//			
//			if(Objects.isNull(aplicacao)) {
//				validation.addError("erro", "Credenciais inválidas");
//				result = erroJsonRetorno();
//				renderJSON(result.toString());
//			}
//			
//			Requisicao requisicao = Requisicao.find("SELECT r FROM Requisicao r WHERE r.token = ? and r.aplicacao.id = ? and r.isActive = ?", authorization, aplicacao.id, true).first();
//			
//			if(Objects.isNull(requisicao)) {
//				validation.addError("erro", "Token inválido!");
//				result = erroJsonRetorno();
//				renderJSON(result.toString());
//			}
//			
//			requisicao.isActive = false;
//			requisicao = requisicao.save();
			
			AccountVO accountVO = SiebelSearchContactIntegration.loadContactByIdToVO(idSiebel);
			
			if(Objects.isNull(accountVO)) {
				validation.addError("erro", "Não foi possível localizar o usuário.");
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			if(Objects.isNull(accountVO.lastLead) || (Objects.isNull(accountVO.lastLead.model) || StringUtils.isBlank(accountVO.lastLead.model))
					|| (Objects.isNull(accountVO.lastLead.version) || StringUtils.isBlank(accountVO.lastLead.version))
					|| (Objects.isNull(accountVO.lastLead.protocol) || StringUtils.isBlank(accountVO.lastLead.protocol))) {
				validation.addError("erro", "Não foi possível localizar as informações do ultímo Lead gerado.");
				result = erroJsonRetorno();
				renderJSON(result.toString());
			}
			
			CorpoVeiculo corpo = null;
			VersaoVeiculo versao = null;
			Cor cor = null;
			Veiculo veiculo = null;
			
			LogEnvioCotacaoMonteOSeu log = LogEnvioCotacaoMonteOSeu.find("SELECT l FROM LogEnvioCotacaoMonteOSeu l WHERE l.jsonSaida like ? order by create_date desc", "%"+accountVO.lastLead.protocol+"%").first();
			
			if(Objects.nonNull(log)) {
				veiculo = Veiculo.findById(log.veiculo.id);
				cor = Cor.findById(log.cor.id);
			} else {
				if((Objects.nonNull(accountVO.lastLead.model) && StringUtils.isNotBlank(accountVO.lastLead.model))
						&& Objects.nonNull(accountVO.lastLead.version) && StringUtils.isNotBlank(accountVO.lastLead.version)) {
					corpo = CorpoVeiculo.find("SELECT c FROM CorpoVeiculo c WHERE c.siebelSendDescription = ? and c.isAtivo = ?", accountVO.lastLead.model, true).first();
					versao = VersaoVeiculo.find("SELECT v FROM VersaoVeiculo v WHERE v.descricao = ? and v.ativo = ?", accountVO.lastLead.version, true).first();
					veiculo = Veiculo.find("SELECT v FROM Veiculo v WHERE v.corpoVeiculo.id = ? AND v.versaoVeiculo.id = ? ORDER BY preco DESC", corpo.id, versao.id).first();					
				} else if((Objects.nonNull(accountVO.lastLead.model) && StringUtils.isNotBlank(accountVO.lastLead.model))
						&& Objects.isNull(accountVO.lastLead.version) && StringUtils.isBlank(accountVO.lastLead.version)) {
					corpo = CorpoVeiculo.find("SELECT c FROM CorpoVeiculo c WHERE c.siebelSendDescription = ? and c.isAtivo = ?", accountVO.lastLead.model, true).first();
					veiculo = Veiculo.find("SELECT v FROM Veiculo v WHERE v.corpoVeiculo.id = ? ORDER BY preco DESC", corpo.id).first();
				} else if((Objects.isNull(accountVO.lastLead.model) && StringUtils.isBlank(accountVO.lastLead.model))
						&& Objects.isNull(accountVO.lastLead.version) && StringUtils.isBlank(accountVO.lastLead.version)) {
					validation.addError("erro", "Não foi possível localizar as informações do ultímo Lead gerado.");
					result = erroJsonRetorno();
					result.addProperty("modelo", accountVO.lastLead.model);
					result.addProperty("versao", accountVO.lastLead.version);
					result.addProperty("cor", accountVO.lastLead.color);
					renderJSON(result.toString());
				}
				
				if(Objects.nonNull(accountVO.lastLead.color) && StringUtils.isNotBlank(accountVO.lastLead.color)) {
					cor = Cor.find("SELECT c FROM Cor c WHERE c.nomeIngles = ? and c.ativo = ? order by create_date desc", accountVO.lastLead.color, true).first();
					
					if(Objects.isNull(cor)) {
						cor = Cor.find("SELECT c FROM Cor c WHERE c.nomeIngles = ? and c.ativo = ? order by create_date desc", "Atlas White", true).first();
					}
				} else {
					cor = Cor.find("SELECT c FROM Cor c WHERE c.nomeIngles = ? and c.ativo = ? order by create_date desc", "Atlas White", true).first();
				}
			}
			
			Dealer dealer = Dealer.find("SELECT d FROM Dealer d WHERE d.csn = ?", dealerCode).first();
			String comentario = carregaComentario(novoDealer);
			String protocolo = SiebelMonteSeuCotacaoController.envioCotacao(idSiebel, 2L, veiculo, cor, dealer, comentario, null, null, null, null);
			
			if(StringUtils.isBlank(protocolo)){
				validation.addError("protocol", Messages.get("contact.send.error"));
				result = getErrorResult(validation.errorsMap());
				renderJSON(result.toString());
			}
			
			result = getSuccessResult();
			result.addProperty("protocolo", protocolo);
			renderJSON(result.toString());
		} catch (Exception e) {
			validation.addError("erro", e.getMessage() + "\nErro compilação de código!");
			result = erroJsonRetorno();
			renderJSON(result.toString());
		}
	}
	
	private static String carregaComentario(boolean novoDealer) {
		String comentario = "";
		if(novoDealer) {
			comentario = "Lead gerado através de Pesquisa de razões de não compra e cliente deseja um contato de sua concessionária. Este cliente já efetuou uma cotação em outra concessionária anteriormente, portanto entenda o momento de compra e ofereça a melhor condição";
		} else {
			comentario = "Lead gerado através de Pesquisa de razões de não compra e cliente deseja um novo contato desde sua última cotação. Verifique o momento de compra deste cliente e ofereça a melhor condiação";
		}
		return comentario;
	}
	
	private static String envioRequisicaoService(String idSiebel, Long idCampanha, String utm_campaign, String utm_content, String utm_medium, String utm_source) {
		UpsertServiceRequestOutput requestOutput = SiebelSSIntegration.sendServiceRequestByDataBaseParametersId(idSiebel, idCampanha, utm_campaign, utm_content, utm_medium, utm_source);
		
		if(requestOutput == null) {
			validation.addError("message", "Erro no envio das informações, por favor tente novamente em instantes!");
			validation.addError("message", "Não foi possível enviar o VOC");
		}
		
		if(requestOutput != null && StringUtils.isNotBlank(requestOutput.getErrorSpcCode()) && !requestOutput.getErrorSpcCode().equals("0")) {
			validation.addError("message", "Erro no envio das informações, por favor tente novamente em instantes!");
			validation.addError("message", requestOutput.getErrorSpcCode() + " - " + requestOutput.getErrorSpcMessage());
		}
		
		String protocolo = requestOutput.getProtocol();
		return protocolo; 
	}
	
	private static void salvaResposta(String texto, String idSiebel, Pesquisa pesquisa, Questao questao, Alternativa alternativa) {
		Resposta resposta = null;
		
		if(questao.tipo.equals(QuestaoTipoEnum.MULTIPLA)) {
			resposta = Resposta.find("SELECT r FROM Resposta r WHERE r.id_siebel = ? AND r.pesquisa.id = ? AND r.questao.id = ? AND r.alternativa.id = ?", idSiebel, pesquisa.id, questao.id, alternativa.id).first();
			
			if(Objects.nonNull(resposta)) {
				resposta.delete();
			}
			
			resposta = new Resposta();
			resposta.resposta = alternativa.descricao.contains("Outro") ? texto : null;
			resposta.id_siebel = idSiebel;
			resposta.pesquisa = pesquisa;
			resposta.questao = questao;
			resposta.alternativa = alternativa;
			resposta = resposta.save();
			
		} else {
			resposta = Resposta.find("SELECT r FROM Resposta r WHERE r.id_siebel = ? AND r.pesquisa.id = ? AND r.questao.id = ?", idSiebel, pesquisa.id, questao.id).first();

			if(Objects.nonNull(resposta)) {
				resposta.resposta = texto;
				resposta.alternativa = alternativa;
				resposta = resposta.save();
			} else {
				resposta = new Resposta();
				resposta.resposta = texto;
				resposta.id_siebel = idSiebel;
				resposta.pesquisa = pesquisa;
				resposta.questao = questao;
				resposta.alternativa = alternativa;
				resposta = resposta.save();
			}
		}
	}
	
	private static List<Alternativa> getAlternativas(Questao questao) {
		List<Alternativa> alternativas = new ArrayList<Alternativa>();
		
		alternativas = Alternativa.find("SELECT a FROM Alternativa a WHERE a.questao.id = ? AND a.ativo = ? AND a.removido = ? ORDER BY a.ordem ASC", questao.id, true, false).fetch();
		return alternativas;
	}
	
	private static Questao getQuestao(Pesquisa pesquisa, Resposta resposta) {
		Questao questao = null;
		
		if(Objects.nonNull(resposta)) {
			Questao ultima_questao = Questao.findById(resposta.questao.id);
			
			if(ultima_questao.ultima) {
				questao = ultima_questao;
			} else {
				questao = Questao.findById(resposta.alternativa.questao_destino.id);
			}
		} else {
			questao = Questao.find("SELECT q FROM Questao q WHERE q.pesquisa.id = ? and q.ativo = ? and q.removido = ? ORDER BY q.ordem ASC", pesquisa.id, true, false).first();
		}
		
		return questao;
	}
	
	private static void validaPesquisa(Pesquisa pesquisa) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		if(Objects.isNull(pesquisa)) {
			validation.addError("erro", "Houve um erro ao carregar as informações da pesquisa.");
		}
		
		if(pesquisa.ativo == false || pesquisa.removido == true) {
			validation.addError("erro", "Está pesquisa não está mais disponível.");
		}
		
		if(pesquisa.data_inicio.after(new Date())) {
			validation.addError("erro", "Está pesquisa ainda não está disponível para ser respondidade, data de ínicio: " + sdf.format(pesquisa.data_inicio) + ".");
		}
		
		if(pesquisa.data_fim.before(new Date())) {
			validation.addError("erro", "Está pesquisa expirou, infelizmente não é mais possível respondê-la. Data limite para respondê-lá: " + sdf.format(pesquisa.data_fim) + ".");
		}
	}

	public static List<Marca> buscaMarcas() {
		List<Marca> marcas = Marca.findAll();
		return marcas;
	}
	
	public static void buscaCarroCotado(String modelo, Long marcaId) {
		List<OutrosVeiculos> outros = new ArrayList<>();
		if(modelo != null && StringUtils.isNotBlank(modelo)) {
			if(modelo.equalsIgnoreCase("Hyundai Creta")) {
				outros = OutrosVeiculos.find("SELECT o FROM OutrosVeiculos o where o.marca.id = ? and o.tag = ? and o.ativo = ? order by o.descricao asc", marcaId, "CRETA", true).fetch();
			} else if (modelo.equalsIgnoreCase("HB20 New Generation")) {
				outros = OutrosVeiculos.find("SELECT o FROM OutrosVeiculos o where o.marca.id = ? and o.tag = ? and o.ativo = ? order by o.descricao asc", marcaId, "HB20", true).fetch();
			} else if (modelo.equalsIgnoreCase("HB20S New Generation")) {
				outros = OutrosVeiculos.find("SELECT o FROM OutrosVeiculos o where o.marca.id = ? and o.tag = ? and o.ativo = ? order by o.descricao asc", marcaId, "HB20S", true).fetch();
			} else if (modelo.equalsIgnoreCase("HB20X New Generation")) {
				outros = OutrosVeiculos.find("SELECT o FROM OutrosVeiculos o where o.marca.id = ? and o.tag = ? and o.ativo = ? order by o.descricao asc", marcaId, "HB20X", true).fetch();
			} else if (modelo.equalsIgnoreCase("HB20 Sport")) {
				outros = OutrosVeiculos.find("SELECT o FROM OutrosVeiculos o where o.marca.id = ? and o.tag = ? and o.ativo = ? order by o.descricao asc", marcaId, "HB20", true).fetch();
			} else if (modelo.equalsIgnoreCase("Creta New Generation")) {
				outros = OutrosVeiculos.find("SELECT o FROM OutrosVeiculos o where o.marca.id = ? and o.tag = ? and o.ativo = ? order by o.descricao asc", marcaId, "CRETA", true).fetch();
			} else {
				outros = OutrosVeiculos.find("SELECT o FROM OutrosVeiculos o where o.ativo = ? order by o.descricao asc", true).fetch();
			}
		} else {
			outros = OutrosVeiculos.find("SELECT o FROM OutrosVeiculos o where o.ativo = ? order by o.descricao asc", true).fetch();
		}
		
		OutrosVeiculos o = new OutrosVeiculos();
		o.descricao = "Outros";
		
		outros.add(o);
		
		render(outros);
	}
	
	private static Dealer buscaDealerCotado(AccountVO accountVO) {
		Dealer dealer = null;
		if(accountVO.lastLead != null && (accountVO.lastLead.csn != null && StringUtils.isNotBlank(accountVO.lastLead.csn))) {
			dealer = Dealer.find("SELECT d FROM Dealer d where d.csn =  ?", accountVO.lastLead.csn).first();
		} else { 
			dealer = new Dealer();
		}
		return dealer;
	}
	
	private static Long bodyToLong(String param, String name) {
    	JsonParser parser = new JsonParser();
		JsonObject jsonEntrada = (JsonObject) parser.parse(param);
		JsonElement tokenJson = jsonEntrada.get(name);
		Long token = tokenJson.getAsLong();
		
		validation.required(name, name).message(Messages.get("validation.required", name));
		
		return token;
	}
	
	private static JsonArray bodyToList(String param, String name) {
    	JsonParser parser = new JsonParser();
		JsonObject jsonEntrada = (JsonObject) parser.parse(param);
		JsonElement tokenJson = jsonEntrada.get(name);
		JsonArray array = tokenJson.getAsJsonArray();
		
		return array;
	}
	
	private static List<Long> converteJsonArray(JsonArray array) {
		List<Long> alternativas = new ArrayList<>();
		
		for (JsonElement json : array) {
			Long token = json.getAsLong();
			alternativas.add(token);
		}
		return alternativas;
	}
	
	private static JsonObject erroJsonRetorno() {
		JsonObject result = new JsonObject();
		result = getErrorResult(validation.errorsMap());
		return result;
	}
}
